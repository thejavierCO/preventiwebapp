import RS from './Service.js';
const rs = new RS({
	require:[
		"./json/model.json",
		"./json/db.json"
	],
	service:"./js/sw.js"
});
const {sw,methods,data} = rs;
sw().then(e=>e).catch(e=>e)
.then(sw=>data()
.then(e=>({file:e,sw}))
.catch(e=>({file:e,sw})))
.then(e=>{
	const {json} = methods, {file,sw} = e, {data} = file;
	const db = new json(data);
	console.log(db.get())
})
export default app;