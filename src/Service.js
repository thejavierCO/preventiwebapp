class json{
    constructor(db){
        let {gettype,getkeys} = tools;
        if(gettype(db)==="list"){
            getkeys(db).map(b=>{
                getkeys(b).map(c=>{
                    if(b[c].options){
                        this.model = b;
                    }else{
                        this.data = b;
                    }
                })
            })
        }else{
            throw db;
        }
        this.get = (type)=>this.setloop(type&&gettype(type)==="string"?this.model[type]:this.model,type&&gettype(type)==="string"?this.data[type]:this.data);
    }
    Body(model,db){
        const {gettype,getkeys} = tools;
        let result = gettype(db)==="key"?{}:gettype(db)==="list"?[]:"";
        gettype(db)==="key"?getkeys(db).map(e=>{
            result[e] = {};
            let t = this.Type(model[e],db[e]);
            if(gettype(db[e])==="key"){
                getkeys(db[e]).map(b=>{
                    let item = db[e][b];
                    let body = this.Type(t[b],item);
                    if(gettype(body)==="key"){
                        switch(gettype(item)) {
                            case "list":
                                result[e][b] = item.map(e=>{
                                    console.log(this.Type(body,e),e)
                                })
                            break;
                            default:
                                console.warn(item);
                            break;
                        }
                    }else{
                        result[e][b] = body;
                    }
                })
            }
        }):gettype(db)==="list"?getkeys(db).map(e=>{
            let type = this.Type(model,e);
            if(type){
                result.push(e);
            }
        }):model?result=db:console.warn(model,db);
        return result;
    }
    Type(model,db){
        const { gettype , getkeys } = tools;
        const { options , body} = model;
        if(body){
            if(gettype(body)==="list"){
                let result;
                body.map(e=>{
                    let item = this.Type({body:e,options},db);
                    if(item!=={}){
                        console.log(item);
                    }
                })
                return result;
            }else if(gettype(body)==="key"){
                console.error(body);
            }else if(gettype(body)==="string"){
                return typeof db === body?options:false;
            }
        }else if(gettype(model)==="string"){
            return typeof db === model?db:false;
        }else if(gettype(model)==="key"){
            if(gettype(db)==="key"){
                getkeys(db).map(b=>{
                    if(model[b]){
                        console.log(db[b]);
                    }
                    console.log(db[b],model[b]);
                })
            }
        }else{
            if(model){
                return db;
            }
        }
    }
    setloop(model,db){
        const {gettype,getkeys} = tools;
        console.log(this.Body(model,db))
    }
}

const tools = {
    sw:(a,b=false)=>'serviceWorker' in navigator?(b?(!navigator.serviceWorker.controller?(a?(navigator.serviceWorker.register(a)):[false,a]):[navigator.serviceWorker.controller.state,navigator.serviceWorker.controller.scriptURL].toString()):a?(navigator.serviceWorker.register(a)):[false,b]
    ):[false,navigator],
    delsw:()=>{
        return new Promise((res,req)=>{const {sw,gettype} = tools;
            if(sw()){
                navigator.serviceWorker.getRegistrations().then(e=>gettype(e)==="list"?{list:e}:{error:e}).then(e=>{let {list,error} = e;if(list){list.map(e=>{e.unregister()});res({status:true});}else if(error){req({status:null})}})
            }else{
                req({status:false});
            }
        })
    },
    ct:()=>'caches' in self?caches:false,
    local:()=>'localStorage' in self?localStorage:false,
    session:()=>'sessionStorage' in self?sessionStorage:false,
    getfile:async (file)=>{
        const {gettype,getkeys,getfile} = tools;
        try{
            if(gettype(file)==="list"){return await getkeys(file).map(async (e)=>await getfile(e));
            }else if(gettype(file)==="string"){return await fetch(file);
            }else{throw file;}
        }catch(err){console.warn(err)}
    },
    getkeys:(a)=>{const{gettype}=tools;if(typeof a === "object"){return gettype(a)==="list"?a:Object.keys(a);}else{throw "require object"}},
    gettype:(a)=>{try{return (a?(typeof a==="object"?a.length?"list":"key":typeof a):typeof a);}catch(e){console.warn([e],a);}},
    createTag:(a)=>document.createElement(a),
    w:(a)=>console.warn(a),
    err:(a)=>console.error(a),
    log:(a)=>console.log(a),
    json
}

function RS(a){
    const {getfile,gettype,getkeys,w,err,log,sw,delsw} = tools; 
    const {require,service} = a;
    if(require?gettype(require)==="list":false){
        this.data = (t=1000)=>new Promise((res,req)=>{
            let data = [],i = 0;
            require.map(e=>{getfile(e).then(e=>e.json()).then(e=>data.push(e)).catch(e=>data.push(e))})
            let time = setInterval(()=>{
                if(data.length===require.length){res({data:data,time:i});clearInterval(time);
                }else if(i>t){req({data:data,time:i});clearInterval(time);}
                i++;
            })
        })
    }
    if(service?gettype(service)==="string":false){
        this.sw = (a)=>new Promise((res,req)=>a?(gettype(a)==="string"?(sw()?(a==="run"?res(sw(service,true)):a==="del"?res(delsw()):req({status:null,msg:a})):req({status:false,msg:sw()})):gettype(a)):req({status:false,msg:"require run or del"}))
    }
    this.methods = tools;
}

export default RS;