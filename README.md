# Proyecto
#### Acerca del proyecto
*Proyecto realizado para ayudar a proporcionar informacion real sobre este acontecimiento que esta ocurriendo en todo el mundo y asi tener un centro de informacion en la que las personas puedan entrar de forma segura y tener apoyo si se encuentran con algún inconveniente*
#### Herramientas utilizadas
 - Svelte
 - Sass
#### Ideas a implementar
- [ ] Noticias actualizadas sobre los acontecimiento que estan ocurriendo
 - [ ] Contenido actualizado en tiempo real
 - [ ] Chat en tiempo real apartir de websocket
 - [ ] Implementacion de chatbot usando websocket